package info.hccis.teetimebooker.bo;

import com.google.gson.Gson;
import info.hccis.teetimebooker.Controller;
import info.hccis.teetimebooker.entity.Booking;
import info.hccis.util.CisUtility;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class will contain the business logic concerned with accessing the
 * bookings.
 *
 * @since 2020-05-19
 * @author bjm
 */
public class BookingBO {

    public static final String PATH = "d:\\bookings\\";
    public static final String FILE_NAME = "bookings.json";

    private ArrayList<Booking> bookings = new ArrayList();

    /**
     * Load the bookings from the file into the bookings ArrayList.
     *
     * @since 2020-05-19
     * @author BJM
     */
    public  void loadBookings() {

        bookings.clear();
        try {
            Path bookingsPath = Paths.get(PATH + FILE_NAME);
            List<String> lines = Files.readAllLines(bookingsPath);
            Gson gson = new Gson();

            //For each line in the file...
            for (String current : lines) {
                //make sure you don't try to create a booking from an empty line.
                if (!current.isEmpty()) {
                    Booking booking = gson.fromJson(current, Booking.class);
                    bookings.add(booking);

                    //If the id of this booking read from the file is > the nextId in the 
                    //Booking class, then set the next id to this booking's id.
                    if (booking.getId() > Booking.getNextId()) {
                        Booking.setNextId(booking.getId());
                    }
                }
            }
        } catch (IOException ex) {
            CisUtility.display("Exception reading bookings from file");
        }

    }

    /**
     * Write all bookings to the file
     *
     * @since 20200519
     * @author BJM
     */
    public void writeAllBookings() {

        //Create a new writer which will override the file containing the bookings.
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(PATH + FILE_NAME, false));
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Booking current : bookings) {
            try {
                //Use GSON to get a json string encoding for a booking
                Gson gson = new Gson();
                String bookingJson = gson.toJson(current);

                writer.write(bookingJson + System.lineSeparator());
            } catch (IOException ex) {
                CisUtility.display("Error with file access");
            }

        }
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Using java nio classes to ensure the bookings file is setup.
     *
     * @since 20200514
     * @author BJM
     */
    public void setupFile() {
        File myFile;
        try {
            Path path = Paths.get(PATH);
            try {
                Files.createDirectories(path);
            } catch (IOException ex) {
                CisUtility.display("Error creating directory");
            }

            myFile = new File(PATH + FILE_NAME);
            if (myFile.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File already exists.");
            }

        } catch (IOException ex) {
            CisUtility.display("Error creating new file");
        }
    }

    /**
     * Update booking
     *
     * @since 2020-05-19
     * @author BJM
     */
    public void updateBooking() {

        loadBookings();

        int id = CisUtility.getInputInt("What id do you want to update?");

        //Identify which one to update...
        for (Booking current : bookings) {
            if (current.getId() == id) {
                //found it!
                CisUtility.display("Booking to update:");
                CisUtility.display(current.toString());
                CisUtility.display("Please provide new details");
                current.getInformtion();
                break;
            }
        }

        //Rewrite all the bookings to the file.
        writeAllBookings();
    }

    /**
     * Read the file and show all the bookings
     *
     * @since 20200514
     * @author BJM
     */
    public void showBookings() {

        bookings.clear();
        loadBookings();

        for (Booking current : bookings) {
            CisUtility.display(current.toString());
        }

    }

    /**
     * Add a booking by writing to the file
     *
     * @since 20200514
     * @author BJM
     */
    public void addABooking() {
        BufferedWriter writer = null;
        try {
            Booking booking = new Booking();
            booking.setNextId(); //Set the id based on the next available id.

            booking.getInformtion();
            //TODO Write the new booking to a file.  (Append)
            //Append to the file.
            writer = new BufferedWriter(new FileWriter(PATH + FILE_NAME, true));

            //Use GSON to get a json string encoding for a booking
            Gson gson = new Gson();
            String bookingJson = gson.toJson(booking);

            writer.write(bookingJson + System.lineSeparator());
            //writer.write(booking.toCSV()+System.lineSeparator());
            writer.close();

            //*******************************
            // Write the object to a file
            //https://attacomsian.com/blog/java-write-object-to-file
            //*******************************
            try (FileOutputStream fos = new FileOutputStream(PATH+"Booking" + booking.getId() + ".dat");
                    ObjectOutputStream oos = new ObjectOutputStream(fos)) {

                // write object to file
                oos.writeObject(booking);

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } catch (IOException ex) {
            CisUtility.display("Error with file access");
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                CisUtility.display("Error closing file");
            }
        }
    }


    
}
