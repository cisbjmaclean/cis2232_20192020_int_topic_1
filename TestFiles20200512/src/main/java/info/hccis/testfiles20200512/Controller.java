package info.hccis.testfiles20200512;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * First project for using files
 *
 * @author bjm
 * @since 20200512
 */
public class Controller {

    public static void main(String[] args) {

        System.out.println("Hello for files");

        Path bookingsPath = Paths.get("D:\\Bitbucket\\bookings.txt");

        try {
            System.out.println("Starting to read the file.");
            List<String> theLines = Files.readAllLines(bookingsPath);

            ArrayList<String> theLinesArrayList = new ArrayList(theLines);

            System.out.println("Finished reading the file.");
            for (String current : theLinesArrayList) {
                System.out.println(current);
            }
            System.out.println("Finished showing the lines to the console.");
            
            //About to write to the file
            System.out.println("About to write to the file");
            writeStringUsingBufferedWriter();
            System.out.println("Finished writing to the file");
        } catch (IOException ex) {
            System.out.println("Error accessing the file: " + ex.getMessage());
        }

    }

    public static void writeStringUsingBufferedWriter() throws IOException {
        
        System.out.println("Enter new entry for the file");
        Scanner input = new Scanner(System.in);
        
        String str = input.nextLine()+System.lineSeparator();
        //Append to the file.
        BufferedWriter writer = new BufferedWriter(new FileWriter("d:\\Bitbucket\\bookings.txt", true));
        writer.write(str);
        writer.close();
    }

}
